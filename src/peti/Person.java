package peti;

import java.time.LocalDate;

public class Person {

    private int age;
    String name;
    public String surname;
    public LocalDate birthday;

    //KONSTRUKTOR
    //OVERLOADING po broju parametara
    public Person(String name) {
        this.name = name;

    }

    //OVERLOADING po tipu

    public Person(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    public Person(String name, String surname, int age, LocalDate birthday) {
        this.name = name;
        this.age = age;
        this.surname = surname;
        this.birthday = birthday;
    }

    public void ubaciStarost(int param) {
        System.out.println("Ubacujem starost osobe");
        this.age = param;

    }
    
  
public void print (){
    System.out.println("Ime: "+name+" , Prezime: "+surname);
    
}
    
    

    
    

}
