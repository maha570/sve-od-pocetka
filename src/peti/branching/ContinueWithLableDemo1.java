package peti.branching;

import javax.swing.JOptionPane;

public class ContinueWithLableDemo1 {

    public static void main(String[] args) {
        String recenica = JOptionPane.showInputDialog("Unesi rečenicu:");
        String rijec = JOptionPane.showInputDialog("Unesi rijec:");
        boolean contains = false;
        int brojac = 0;
        int max = recenica.length() - rijec.length();
        labeliranaPEtlja:
        for (int i = 0; i <= max; i++) {
            int k = i;
            for (int j = 0; j < rijec.length(); j++) {
                if (recenica.charAt(k++) != rijec.charAt(j)) {
                    continue labeliranaPEtlja;

                }
                contains = true;
                break labeliranaPEtlja;
            }
            System.out.println(contains ? "Sadruana" : "Nije sadrzana rijec u recenici");

        }
    }

}
