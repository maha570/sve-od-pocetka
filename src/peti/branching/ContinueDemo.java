package peti.branching;

import javax.swing.JOptionPane;

public class ContinueDemo {

    public static void main(String[] args) {
        String recenica = JOptionPane.showInputDialog("Unesi rečenicu");
        String slovo = JOptionPane.showInputDialog("Unesi slovo");
        if (slovo.length() != 1 && slovo.length() != 0) {

        } else {
            int count = 0;
            int i = 0;
            for (i = 0; i < recenica.length(); i++) {
                char slovoURecenici = recenica.charAt(i);
                if(slovoURecenici != slovo.charAt(0)){
                    continue;
                }
                count++;

            }
            JOptionPane.showMessageDialog(null, "Slovo u recenici se pojavljije '" +count+"'puta");
        }

    }

}
