package cetvrti;
//Tipovi podataka
//Slozeni
//Prosti
//TIP ime_memorijskog_prostora (varijabla ili objekat) = VVRIJEDNOST

public class ArrayDemo {
    public static void main(String[] args) {
        //SLOŽENI TIP
        //NIZ -> prvi slozeni tip podatka -> new operator slozeni tip
        int [] numbers = new int [3];
        numbers [0] = 23;
        numbers[1] = 2324;
        numbers [2] = 12;
        //Nacin kreiranja niza
        int [] brojevi = {23, 2324, 12};
        
        System.out.println(numbers[1]);
        System.out.println(brojevi[2]);
        
    }
}
