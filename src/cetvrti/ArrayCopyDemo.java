package cetvrti;

public class ArrayCopyDemo {
    public static void main(String[] args) {
        char[] copyFrom ={'d', 'e', 'c', 'a', 'f', 'e', 't', 'i', 'n', 'a', 'f', 'r'};
        char[] copyToArray = new char[7];
        System.out.println("COPY TO prije kopiranja vrijednosti iz copyFrom");
        System.out.println(new String(copyToArray));
        System.arraycopy(copyFrom, 2, copyToArray, 0, 7);
        System.out.println("COPY TO poslije kopiranja iz copyFrom");
        System.out.println(new String (copyToArray));
        
        
    }
}
