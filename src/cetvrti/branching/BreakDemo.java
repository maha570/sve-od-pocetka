package cetvrti.branching;

import java.util.Scanner;

public class BreakDemo {
    public static void main(String[] args) {
        int [] numbers = {1123, 23, 34, 556, 78, 98, 90, ',', 234, 23, 454, 6, 6};
        int searchFor = new Scanner(System.in).nextInt();
        int i;
        boolean pronadjen = false;
        for(i = 0; i<numbers.length; i++){
           int mutant = numbers[i];
           if(mutant == searchFor){
               pronadjen = true;
               break;
           }
        }
        if(pronadjen){
            System.out.println("Pronadjen "+ searchFor +" na indeksu '" +i+ "'");
        }else{
            System.out.println("Nema tog broja kojeg trazis");
        }
        
        
    }
            

}
