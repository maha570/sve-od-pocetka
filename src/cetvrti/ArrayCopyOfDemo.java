package cetvrti;

import java.util.Arrays;

public class ArrayCopyOfDemo {
    public static void main(String[] args) {
        char[] copyForm = {'d', 'e', 'c', 'a', 'f', 'e', 't', 'i', 'n', 'a', 'f', 'r'};
        char[] copyToArray = Arrays.copyOfRange(copyForm, 2, 9);
        System.out.println(new String(copyToArray) );
    }
}
