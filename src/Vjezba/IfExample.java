package Vjezba;

public class IfExample {
   
    public static void main(String[] args) {
        
        double marks = 69.9;
        
        if(marks < 50){
            System.out.println("Nedovoljan");
     
        }else if(marks >=50 && marks < 60){
            System.out.println("Dovoljan");
            
           
        }else if (marks >=60 && marks < 70){
            System.out.println("Dobar");
            
        }else if (marks >=70 && marks < 80){
            System.out.println("Vrlo Dobar");
            
        }else if (marks >=90 && marks < 100){
            System.out.println("Odličan");
            
        }else {
            System.out.println("Pogrešan unos");
        }
  
    }
    
}
