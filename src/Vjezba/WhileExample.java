package Vjezba;

public class WhileExample {

    public static void main(String[] args) {
        int x = 0;
        while (x <= 10) {
            System.out.println("x is: " + x);
            x++;
        }
    }

}
