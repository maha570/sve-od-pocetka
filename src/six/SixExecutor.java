package six;

import six.person.Person;

public class SixExecutor {

    public static void main(String[] args) {
        int number = 23;
        number = 12;
        int[] numbers = new int[5];
        numbers[0] = 23;
        numbers[1] = 12;
        Person.printCounter();
        Person person1 = new Person("Kanita", "Muzaferija");
        Person person2 = new Person("Ferida", "Bobar");

        person1.printPerson();
        person1.printCounter();
        person2.printPerson();
        person2.printCounter();
        Person person3 = new Person ("Marko", "Nikolic");
        person2.printCounter();
        person1.printCounter();
        person3.printCounter();
        
    }

}
