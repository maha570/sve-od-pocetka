package six.person;

public class PersonExecutor {
    public static void main(String[] args) {
        Person p1 = new Person("Vedad", "Efendic");
        Person p2 = new Student("Fadil", "Turalic", 30);
        String fadilName = p2.name;
        String fadilSurname = p2.surname;
        String res1 = checkPerson(p1);
        System.out.println(res1);
        String res2 = checkPerson(p2);
        System.out.println(res2);
        
    }
    static String checkPerson(Person p){
        if (p instanceof Student){
            return "STUDENT";
        }else if(p instanceof Teacher){
            return "TEACHER";
        }else{
            return "OBICNA PERSONA";
        }
        
    }

}
