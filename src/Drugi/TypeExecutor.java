package Drugi;

public class TypeExecutor {
    public static void main(String[] args) {
        boolean condiction = false;
        byte b = 100;
        short shortNumber = 1000;
        //automatska konverzija
        int intNumber = shortNumber;
       //eksplicitna konverzija
        short shortNumber1 = (short) intNumber;
        int number1 = 2147483647;
        long veciBroj = 2147483648L;
        float decimalniBroj = 34.1F;
        double doubleBroj = 34.0;
        char varijabla = 'd';
        long bankAccount = 161_365_2564575L;
        String text = "Cao";
        
        int hexValue = 0x1a;
        System.out.println(hexValue);
        int binarniBroj = 0b11010;
        System.out.println(binarniBroj);
    }

}
