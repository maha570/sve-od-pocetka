package Treci;

import javax.swing.JOptionPane;

public class SwitchDemo {

    public static void main(String[] args) {
        String korisničkiUnos = JOptionPane.showInputDialog("Unesi molim te broj");
        int uneseniBroj = Integer.parseInt(korisničkiUnos);
        String imedanaUSedmici = "Unknown";
        switch (uneseniBroj) {
            case 1:
                imedanaUSedmici = "Ponedjeljak";
                break;
            case 2:
                imedanaUSedmici = "Utorak";
                break;
            case 3:
                imedanaUSedmici = "Srijeda";
                break;
            case 4:
                imedanaUSedmici = "Četvrtak";
                break;
            case 5:
                imedanaUSedmici = "Petak";
                break;
            case 6:
                imedanaUSedmici = "Subota";
                break;
            case 7:
                imedanaUSedmici = "Nedjelja";
                break;
            default:
                imedanaUSedmici = "Mama sedmica ima sedam dana i zato se zove sedmica";
                break;
        }
        System.out.println("Ime dana u sedmici koje odgovara broju " +uneseniBroj+" je: "+imedanaUSedmici);

    }

}
